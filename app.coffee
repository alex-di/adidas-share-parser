cluster = require "cluster"
http = require "http"

if cluster.isMaster

  mongoose = require "mongoose"
  Schema = mongoose.Schema

  mongoose.connect "mongodb://localhost:27017/charge"

  pool = {}

  Charge = new Schema
    user: String
    type: {
      type: Number
      default: 0
    }
    sent:
      type: Boolean
      default: false

    sentDate: Date

  Media = new Schema
    location:

      latitude:
        type: String
        default: 0

      longitude:
        type: String
        default: 0

    charging: [Charge]

    user: {
      id: Number
      profile_picture: String
      username: String
      full_name: String
    }

    link: String

    createdTime:
      type: Date
      default: Date.now

    instagramId: String
    images: {}

    last: String

    approved: {
      type: Number
      default: 0
    },
    { versionKey: false }

  Media = mongoose.model "Media", Media

  Media.find {}
  .distinct "charging.user", (err, result) ->
      console.log result.length
      r =
        vk: 0
        fb: 0
        tw: 0


      checkEnd = ->
        end = true
        for p, i of pool
          end = false unless i

        console.log r

        if end
          console.log "end"
          process.kill()

      url = "http://energy.adidas-running.ru"

      http.get "http://vk.com/share.php?act=count&index=1&url=" + url, (res) ->
        dt = ""
        res.on "data", (chunk) ->
          dt += chunk

        res.on "end", ->
          num = parseInt dt.replace /VK\.Share\.count\(1,\s(\d*)\);/, "$1"
          console.log "vk", num
          r.vk += num

      http.get "http://graph.facebook.com/?id=" + url, (res) ->
        dt = ""
        res.on "data", (chunk) ->
          dt += chunk

        res.on "end", ->
          dt = JSON.parse dt
          num = dt.shares || 0
          console.log "fb", num
          r.fb += num


      http.get "http://cdn.api.twitter.com/1/urls/count.json?url=" + url, (res) ->
        dt = ""
        res.on "data", (chunk) ->
          dt += chunk

        res.on "end", ->
          dt = JSON.parse dt
          num = dt.count || 0
          console.log "tw", num
          r.tw += num

      cluster.on "exit", (worker, pid, sig) ->
        pool[worker.process.pid] = true
        checkEnd() if result.length is 0

      timer = setInterval ->
        name = result.pop()
        unless name?
          clearInterval timer
          return

        name = name.replace "@", ""

        w = cluster.fork()
        w.send name
        w.on "message", (msg) ->
          if msg.type is "kill"
            c = msg.count
            r.vk += c.vk1
            r.fb += c.fb1
            r.tw += c.tw1

            @kill()

        pool[w.process.pid] = false

      , 100


else
  results =
    vk1: null
    tw1: null
    fb1: null

  checkRes = (type, num) ->
    results[type] = num
    kill = true
    n = 0
    for t, v of results
      unless v?
        kill = false

    if kill
      process.send {type: "kill", count: results}


  process.on "message", (msg) ->
    url = "http://energy.adidas-running.ru/user/" + msg
    console.log url

    http.get "http://vk.com/share.php?act=count&index=1&url=" + url, (res) ->
      dt = ""
      res.on "data", (chunk) ->
        dt += chunk

      res.on "end", ->
        num = parseInt dt.replace /VK\.Share\.count\(1,\s(\d*)\);/, "$1"

        checkRes "vk1", num

    http.get "http://graph.facebook.com/?id=" + url, (res) ->
      dt = ""
      res.on "data", (chunk) ->
        dt += chunk

      res.on "end", ->
        dt = JSON.parse dt
        num = dt.shares || 0
        checkRes "fb1", num

    http.get "http://cdn.api.twitter.com/1/urls/count.json?url=" + url, (res) ->
      dt = ""
      res.on "data", (chunk) ->
        dt += chunk

      res.on "end", ->
        dt = JSON.parse dt
        num = dt.count || 0
        checkRes "tw1", num